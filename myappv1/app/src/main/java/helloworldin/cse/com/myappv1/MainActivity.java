package helloworldin.cse.com.myappv1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.view.View;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button greetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);
        greetButton = (Button) findViewById (R.id.greetButton);
        greetButton.setOnClickListener (this);

    }

    @Override
    public void onClick(View v) {
        TextView textMessage = (TextView) findViewById (R.id.textView);
        EditText editFriendName = (EditText) findViewById (R.id.editFriendName);
        String friendName = editFriendName.getText().toString();
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        // textMessage.setText("cal"+ "hour"+hour);
        // textMessage.setText(friendName);
        if(hour>= 12 && hour < 17){
            textMessage.setText("Good Afternoon"+friendName+"!!");
        } else if(hour >= 17 && hour < 21){
            textMessage.setText("Good Evening"+friendName+"!!");
        } else if(hour >= 21 && hour < 24){
            textMessage.setText("Good Night"+friendName+"!!");
        } else {
            textMessage.setText("Good Morning "+friendName+"!!");
        }
    }
}




